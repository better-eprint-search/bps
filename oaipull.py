import requests
import json
from pyquery import PyQuery as pq
import os
import time

# xapian in docker lives in the wrong subdir
import sys
sys.path.append('/usr/lib/python3.11/site-packages')
sys.path.append('/usr/lib/python3/dist-packages/')
import dbutils
import xapian

from config import SQL_PAPERS_DB_PATH as sqlite_db
from config import XAPIAN_PAPERS_DB_PATH as xapian_db
from config import LAST_UPDATED
# set a limit on the number of articles to pull
from config import MAX_COUNT
from config import SCRAPE_NON_OAI_METADATA


def get_oai_client():
    # set the oai metadata parser
    URL = 'https://eprint.iacr.org/oai'
    from oaipmh.client import Client
    from oaipmh.metadata import MetadataRegistry, oai_dc_reader
    registry = MetadataRegistry()
    registry.registerReader('oai_dc', oai_dc_reader)
    client = Client(URL, registry)
    return client


def scrape_non_oai_metadata(url):
    resp = requests.get(url)
    authors_json = []
    if resp.ok:
        webpage = pq(resp.text)
        for author in webpage(".author"):
            # author's name
            author_name = pq(author)('.authorName').text()
            # print(author_name)
            # affiliation
            author_affiliation_names = list(filter(lambda x: x != '', pq(author)('.affiliation').text().split(', ')))
            author_affiliations = ', '.join(author_affiliation_names)
            # orcid
            orcid = None
            for link in pq(author)("a"):
                try:
                    href = link.attrib["href"]
                    if 'orcid.org' in href:
                        orcid = href.split('/')[-1]
                        break
                except:
                    continue
            authors_json.append({
                'name': author_name,
                'affiliations': author_affiliations,
                'orcid': orcid
            })
        # venue
        publication_info = None
        for metadata_heading in webpage("#metadata")("dt"):
            try:
                if "Publication info" in metadata_heading.text:
                    publication_info = metadata_heading.getnext().text
                    break
            except:
                continue
        return authors_json, publication_info
    else:
        return [], None


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
                    prog='oaipull',
                    description='pulls eprint papers from oai-pmh service')
    parser.add_argument('-c', '--count', type=int, default=MAX_COUNT, help='maximum number of papers to pull')
    args = parser.parse_args()
    print(f"Pulling {args.count} papers")

    # open the dbs
    if not os.path.isfile(sqlite_db):
        dbutils.setup_sqlite_db(dbpath=sqlite_db)

    con = dbutils.get_db_connection(sqlite_db)
    cur = con.cursor()
    xdb = xapian.WritableDatabase(xapian_db, xapian.DB_CREATE_OR_OPEN)

    # save time of begin of last update
    curtime = time.strftime("%Y-%m-%d, %H:%M UTC", time.gmtime())

    # pull the first args.count papers
    client = get_oai_client()
    count = 0
    for record in client.listRecords(metadataPrefix='oai_dc'):
        if count >= args.count:
            break

        count += 1

        if not record[1]:
            # withdrawn paper
            continue
            print(record[0].identifier())

        url = record[1].getField('identifier')[0]
        year, number = tuple(url.split('/')[-2:])
        title = '' if len(record[1].getField('title')) == 0 else record[1].getField('title')[0]
        abstract = '' if len(record[1].getField('description')) == 0 else record[1].getField('description')[0]
        subject = '' if len(record[1].getField('subject')) == 0 else record[1].getField('subject')[0]
        authors = record[1].getField('creator')
        dates = record[1].getField('date')
        filetype = 'ps' if requests.head(f'{url}.ps').status_code == 200 else 'pdf'
        venue = 'preprint'

        # check if already present in DB
        res = cur.execute("SELECT COUNT(1) FROM papers WHERE url = '{}' ".format(url))
        already_in_table = bool(res.fetchall()[0][0])

        print()
        print('{:33} {} {:120}'.format(url, dates, title[:120]))
        # print('{:33} | {:80} | {:}'.format(url, title[:80], abstract[:60]))

        if already_in_table:
            # check if last_revision field changed, and update if necessary
            res = cur.execute("SELECT last_revision FROM papers WHERE url = '{}' ".format(url))
            stored_last_revision = res.fetchall()[0][0]
            oai_last_revision = dates[1]
            if oai_last_revision == stored_last_revision:
                continue

        # either not in table, or needs to be updated

        # # collect some more data not available in OAI
        authors_json = []
        publication_info = None
        if SCRAPE_NON_OAI_METADATA:
            authors_json, publication_info = scrape_non_oai_metadata(url)

        # # # if either no pull for affiliation was made, or if the pull failed due to the info not bein available:
        if len(authors_json) == 0:
            authors_names = record[1].getField('creator')
            for name in authors_names:
                authors_json.append({'name': name, 'affiliations': '', 'orcid': None})
        if publication_info:
            venue = publication_info

        # # final paper object
        fields = {
            'url': url,
            'year': year,
            'number': number,
            'title': title,
            'abstract': abstract,
            'subject': subject,
            'authors': json.dumps(authors_json),
            'countries': '',
            'first_uploaded': dates[0],
            'last_revision': dates[1],
            'filetype': filetype,
            'venue': venue
        }

        if already_in_table:
            # update sqlite db entry
            cur.execute("UPDATE papers SET title = :title, abstract = :abstract, subject = :subject, authors = :authors, countries = :countries, last_revision = :last_revision, venue = :venue WHERE url = :url", fields)
            con.commit()
            # update xapian db entry
            try:
                print(f"Xapian updating paper: {year}-{number}")
                xpaperid = dbutils.get_paper_unique_identifier({'year': year, 'number': number})
                # xdocid = dbutils.get_xapian_docid(xdb, xpaperid)
                # xdoc = xdb.get_document(xdocid)
                xidterm, new_xdoc = dbutils.create_xapian_doc(fields)
                xdb.replace_document(xidterm, new_xdoc)
            except Exception as e:
                print(f"Issues updating paper with id {xpaperid}")
                raise(e)
        else:
            # not seen before, add to db
            cur.execute("INSERT INTO papers VALUES(:url, :year, :number, :title, :abstract, :subject, :authors, :countries, :first_uploaded, :last_revision, :filetype, :venue)", fields)
            con.commit()

            # insert into xapian db
            xidterm, new_xdoc = dbutils.create_xapian_doc(fields)
            xdb.replace_document(xidterm, new_xdoc)
            print(f"Inserting paper: {year}-{number} ({filetype})")

    con.close()

    # save after finishing update
    with open(LAST_UPDATED, "w") as f:
        f.write(curtime)
