MAX_COUNT = 9999999
SCRAPE_NON_OAI_METADATA = True
ITEMS_PER_PAGE = 50
ALLOWED_FIELDS = [
    "title",
    "authors",
    "abstract"
]
ALLOWED_PRIORITISE_ADV = [
    "relevance",
    "newest",
    "oldest"
]
STATS_DB_PATH = "databases/traffic_stats.db"
SQL_PAPERS_DB_PATH = "databases/eprintdb.sqlite"
XAPIAN_PAPERS_DB_PATH = "databases/eprintdb.xapian"
PW_HASH_PATH = "databases/pw_hash.txt"
LAST_UPDATED = "databases/last_updated.txt"