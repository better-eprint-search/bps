import dbutils
from config import ITEMS_PER_PAGE
try:
    from config import DEBUG
except ImportError:
    DEBUG = False

def generic_search_sql_filter(search_params):
    # parse params
    searchstr = search_params['searchstr']
    field = search_params['field']

    # prepare sql query filter
    if field == "any":
        sql_filter = f"title LIKE '%{searchstr}%' or authors LIKE '%{searchstr}%' or abstract LIKE '%{searchstr}%'"
    else:
        sql_filter = f"{field} LIKE '%{searchstr}%'"
    return sql_filter


def advanced_search_sql_filter(search_params):
    # parse params
    title = search_params['title']
    titlebl = search_params['titlebl']
    author = search_params['author']
    authorbl = search_params['authorbl']
    abstract = search_params['abstract']
    abstractbl = search_params['abstractbl']
    minyear = search_params['minyear']
    maxyear = search_params['maxyear']
    page = search_params['page']

    # prepare an advanced sql filter for the parameters
    sql_filter = f"title LIKE '%{title}%' AND authors LIKE '%{author}%' AND abstract LIKE '%{abstract}%' "
    for bad in titlebl:
        sql_filter += f"AND title NOT LIKE '%{bad}%' "
    for bad in authorbl:
        sql_filter += f"AND authors NOT LIKE '%{bad}%' "
    for bad in abstractbl:
        sql_filter += f"AND abstract NOT LIKE '%{bad}%' "
    sql_filter += f"AND year >= {minyear} and year <= {maxyear} "

    if DEBUG: print('sql filter:', sql_filter)

    return sql_filter


def count_search_res_sql(sql_filter):
    # prepare query
    query = "SELECT COUNT(DISTINCT url) FROM papers WHERE " + sql_filter

    # execute query
    conn = dbutils.get_db_connection()
    count = conn.execute(query).fetchone()[0]
    conn.close()
    return count


def exec_search_sql(sql_filter, page, prioritise=None):
    # first count results
    total_number_of_results = count_search_res_sql(sql_filter)

    tot_pages = (total_number_of_results + ITEMS_PER_PAGE - 1)//ITEMS_PER_PAGE
    if DEBUG: print(f"tot_pages: {tot_pages}")

    # normalise page number
    if page < 0:
        page = 0
    if page > tot_pages - 1: # counting pages from zero
        page = tot_pages - 1

    # prepare query
    query = "SELECT DISTINCT url, title, authors, abstract, first_uploaded, last_revision, year, number, filetype, venue FROM papers WHERE "
    query += sql_filter
    query += " ORDER BY first_uploaded "
    if prioritise != "oldest": # meaning no preference, or newest, or "relevance" which does not work in sqlite
        query += " DESC "
    query += f"LIMIT {ITEMS_PER_PAGE} OFFSET {page * ITEMS_PER_PAGE}"
    if DEBUG: print(f"final query:\n{query}")

    # execute query
    conn = dbutils.get_db_connection()
    papers_rows = conn.execute(query).fetchall()
    conn.close()

    papers = dbutils.sanitise_papers_rows(papers_rows)
    return papers, page, tot_pages


def exec_generic_search_sql(search_params):
    sql_filter = generic_search_sql_filter(search_params)
    return exec_search_sql(sql_filter, search_params['page'])


def exec_advanced_search_sql(search_params):
    sql_filter = advanced_search_sql_filter(search_params)
    return exec_search_sql(sql_filter, search_params['page'], prioritise=search_params['prioritise'])
