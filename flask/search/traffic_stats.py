import os
import argparse
import argon2
from flask import redirect, request
from flask_sqlalchemy import SQLAlchemy
from flask_statistics import Statistics

from config import STATS_DB_PATH, PW_HASH_PATH
try:
    from config import DEBUG
except ImportError:
    DEBUG = False

def check_if_user_is_allowed():
    # check if user is allowed to see statistics page
    with open(PW_HASH_PATH) as f:
        pw_hash = f.readline()

    pw = request.cookies.get('pw')

    if pw:
        ph = argon2.PasswordHasher()
        try:
            allowed = ph.verify(pw_hash, pw)
            if DEBUG: print("password verifies")
        except argon2.exceptions.VerifyMismatchError:
            allowed = False
            if DEBUG: print("password fails")

    if not pw or not allowed:
        return redirect("/search", code=302)


def create_stats_db(db_path=STATS_DB_PATH):
    # create stats database
    import sqlite3
    con = sqlite3.connect(db_path)
    cur = con.cursor()
    cur.execute("CREATE TABLE request(\
                \"index\" INTEGER PRIMARY KEY AUTOINCREMENT,\
                response_time FLOAT,\
                date DATETIME,\
                method TEXT,\
                size INTEGER,\
                status_code INTEGER,\
                path TEXT,\
                user_agent TEXT,\
                remote_address TEXT,\
                exception TEXT,\
                referrer TEXT,\
                browser TEXT,\
                platform TEXT,\
                mimetype TEXT\
    )")
    con.close()


def enable_statistics(app, db_path=STATS_DB_PATH):

    if not os.path.isfile(PW_HASH_PATH):
        if DEBUG: print(f"WARNING: no password file ({PW_HASH_PATH}), disabling traffic stats")
        return

    if not os.path.isfile(db_path):
        try:
            create_stats_db()
        except:
            if DEBUG: print(f"WARNING: stats DB ({db_path}) could not be created, disabling traffic stats")
            return

    app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{db_path}"

    db = SQLAlchemy(app)
    class Request(db.Model):
        __tablename__ = "request"

        index = db.Column(db.Integer, primary_key=True, autoincrement=True)
        response_time = db.Column(db.Float)
        date = db.Column(db.DateTime)
        method = db.Column(db.String)
        size = db.Column(db.Integer)
        status_code = db.Column(db.Integer)
        path = db.Column(db.String)
        user_agent = db.Column(db.String)
        remote_address = db.Column(db.String)
        exception = db.Column(db.String)
        referrer = db.Column(db.String)
        browser = db.Column(db.String)
        platform = db.Column(db.String)
        mimetype = db.Column(db.String)

    statistics = Statistics(app, db, Request, check_if_user_is_allowed)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='If run as standalone, it creates the password hash for authentication.')
    parser.add_argument('password')
    args = parser.parse_args()

    ph = argon2.PasswordHasher()
    pw_hash = ph.hash(args.password)
    with open(PW_HASH_PATH, "w") as f:
        f.write(pw_hash)
    print(f"Password hash stored at {PW_HASH_PATH}")
