import json
import re
import dbutils

from config import MAX_COUNT, ITEMS_PER_PAGE
try:
    from config import DEBUG
except ImportError:
    DEBUG = False

def exec_generic_search_xapian(search_params):
    # parse params
    searchstr = search_params['searchstr']
    field = search_params['field']
    prioritise_latest = search_params['prioritise-latest']
    page = search_params['page']

    # prepare xapian query
    if field == "any":
        querystring = searchstr
        prefixes = None
    else:
        querystring = f"{field}:({searchstr})"
        prefixes = [field]

    # replace two or more consecutive periods, to avoid conflicts with the range query engine in xapian
    querystring = re.sub("\.\.+", " ", querystring)

    if DEBUG: print("querystring:", querystring)

    enquire = dbutils.xapian_search(querystring, logical_op=dbutils.xapian.Query.OP_OR, prefixes=prefixes, prioritise="newest" if prioritise_latest else "relevance")
    mset = enquire.get_mset(0, MAX_COUNT)

    # first count results
    total_number_of_results = mset.size()
    tot_pages = (total_number_of_results + ITEMS_PER_PAGE - 1)//ITEMS_PER_PAGE
    if DEBUG: print(f"tot_pages: {tot_pages}")

    # normalise page number
    if page < 0:
        page = 0
    if page > tot_pages - 1: # counting pages from zero
        page = tot_pages - 1

    # # And print out something about each match
    papers_rows = []
    count = 0
    # for match in enquire.get_mset(page * ITEMS_PER_PAGE, ITEMS_PER_PAGE):
    for match in mset:
        count += 1
        if count < page * ITEMS_PER_PAGE:
            continue
        if count >= (page+1) * ITEMS_PER_PAGE:
            break
        paper = json.loads(match.document.get_data().decode('utf8'))
        papers_rows.append(paper)

    papers = dbutils.sanitise_papers_rows(papers_rows, generator=False)

    return papers, page, tot_pages


def exec_advanced_search_xapian(search_params):
    # parse params
    title = search_params['title']
    titlebl = search_params['titlebl']
    author = search_params['author']
    authorbl = search_params['authorbl']
    abstract = search_params['abstract']
    abstractbl = search_params['abstractbl']
    prioritise = search_params['prioritise']
    minyear = search_params['minyear']
    maxyear = search_params['maxyear']
    page = search_params['page']

    # prepare xapian query
    querystring = ""
    prefixes = []
    if title != "":
        querystring += f"title:({title}) "
        prefixes.append("title")
    if author != "":
        querystring += f"authors:({author}) "
        prefixes.append("authors")
    if abstract != "":
        querystring += f"abstract:({abstract}) "
        prefixes.append("abstract")

    for bad in titlebl:
        querystring += f"-title:({bad}) "
        prefixes.append("title")
    for bad in authorbl:
        querystring += f"-authors:({bad}) "
        prefixes.append("authors")
    for bad in abstractbl:
        querystring += f"-abstract:({bad}) "
        prefixes.append("abstract")

    # replace two or more consecutive periods, to avoid conflicts with the range query engine in xapian
    querystring = re.sub("\.\.+", " ", querystring)

    # now that we escaped from ranges, we can add an id range (cf. dbutils.create_xapian_doc)
    querystring += f"yy{minyear * 100000}..{(maxyear+1) * 100000} "

    if DEBUG: print("querystring:", querystring)

    # remove doubles in prefixes
    prefixes = list(set(prefixes))

    # handle prioritisation kind in xapian_search paramlist
    # raise NotImplementedError("prioritisation not handled")

    enquire = dbutils.xapian_search(querystring, logical_op=dbutils.xapian.Query.OP_AND, prefixes=prefixes, prioritise=prioritise)
    mset = enquire.get_mset(0, MAX_COUNT)

    # first count results
    total_number_of_results = mset.size()
    tot_pages = (total_number_of_results + ITEMS_PER_PAGE - 1)//ITEMS_PER_PAGE
    if DEBUG: print(f"tot_pages: {tot_pages}")

    # normalise page number
    if page < 0:
        page = 0
    if page > tot_pages - 1: # counting pages from zero
        page = tot_pages - 1

    # # And print out something about each match
    papers_rows = []
    count = 0
    # for match in enquire.get_mset(page * ITEMS_PER_PAGE, ITEMS_PER_PAGE):
    for match in mset:
        count += 1
        if count < page * ITEMS_PER_PAGE:
            continue
        if count >= (page+1) * ITEMS_PER_PAGE:
            break
        paper = json.loads(match.document.get_data().decode('utf8'))
        papers_rows.append(paper)

    papers = dbutils.sanitise_papers_rows(papers_rows, generator=False)

    return papers, page, tot_pages
