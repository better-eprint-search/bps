import os
import json
import datetime
from flask import Flask, render_template, request, jsonify, redirect
from search import exec_generic_search, exec_advanced_search

# settings
from config import ALLOWED_FIELDS, ALLOWED_PRIORITISE_ADV, LAST_UPDATED
try:
    from config import STATISTICS_API_KEY
    TRACK_STATISTICS = True
except ImportError:
    TRACK_STATISTICS = False
    STATISTICS_API_KEY = None
try:
    from config import DEBUG
except ImportError:
    DEBUG = False
# from traffic_stats import enable_statistics


def tentative_split(string, delimiter):
    try:
        l = [s.strip() for s in string.split(delimiter)]
    except:
        l = []
    return l


def render_papers(papers, page, tot_pages):
    """
    This code mostly handles the pagination layouting
    """
    # use page and total_number_of_results to construct the list of pagination items
    # <a class="pagination-link" aria-label="Goto page 1">1</a>
    # <span class="pagination-ellipsis">&hellip;</span>
    # <a class="pagination-link" aria-label="Goto page 45">45</a>
    # <a class="pagination-link is-current" aria-label="Page 46" aria-current="page">46</a>
    # <a class="pagination-link" aria-label="Goto page 47">47</a>
    # <span class="pagination-ellipsis">&hellip;</span>
    # <a class="pagination-link" aria-label="Goto page 86">86</a>

    # the following code has to figure out the ellipses, if any
    has_ellipses_pre = bool(page >= 3)
    has_ellipses_post = bool(tot_pages >= 4 and abs(tot_pages - 1 - page) > 2)

    # figure out pagination components
    pagination_cur = page + 1

    pagination_pre = []
    if pagination_cur != 1:
        pagination_pre.append(1)
        if has_ellipses_pre:
            pagination_pre.append('...')
        if page > 1:
            pagination_pre.append(page)

    pagination_post = []
    if pagination_cur != tot_pages:
        pagination_post.append(tot_pages)
        if has_ellipses_post:
            pagination_post.append('...')
        if page < tot_pages - 2:
            pagination_post.append(page+2)
    pagination_post = pagination_post[::-1]

    try:
        with open(LAST_UPDATED) as f:
            last_updated = f.readline()
    except:
        last_updated = None

    return render_template('index.html', papers=papers, pagination=[pagination_pre, pagination_cur, pagination_post], last_updated=last_updated, track_statistics=TRACK_STATISTICS, statistics_api_key=STATISTICS_API_KEY)


app = Flask(__name__)
# enable_statistics(app)


@app.route('/')
def index():
    return redirect("/search", code=302)


@app.route('/about')
def about():
    return render_template('about.html', track_statistics=TRACK_STATISTICS, statistics_api_key=STATISTICS_API_KEY)


@app.route('/limitations')
def limitations():
    return render_template('limitations.html', track_statistics=TRACK_STATISTICS, statistics_api_key=STATISTICS_API_KEY)


@app.route('/search', methods = ['GET'])
def search():
    if request.method == 'POST':
        data = json.loads(request.data)
    else:
        data = dict(request.args)
    if DEBUG: print(data)

    searchstr = '' if 'searchstr' not in data else data['searchstr'].strip()
    field = 'any' if 'field' not in data else data['field']
    prioritise_latest = True if 'prioritise-latest' not in data else (True if data['prioritise-latest'] == 'true' else False)
    if field not in ALLOWED_FIELDS: field = 'any'
    try:
        page = int(data['page'])
    except:
        page = 0

    if DEBUG: print(f"{field}: `{searchstr}`")

    search_params = {
        'searchstr': searchstr,
        'field': field,
        'prioritise-latest': prioritise_latest,
        'page': page
    }

    papers, page, tot_pages = exec_generic_search(search_params, backend="xapian")
    # for paper in papers:
    #     print(paper['title'], end="\n\n")

    if request.method == 'POST':
        return jsonify(papers)

    return render_papers(papers, page, tot_pages)


@app.route('/advanced', methods = ['GET'])
def advanced():
    if request.method == 'POST':
        data = json.loads(request.data)
    else:
        data = dict(request.args)
    if DEBUG: print(data)

    # parse input parameters for the search
    title = '' if 'title' not in data else data['title'].strip()
    author = '' if 'author' not in data else data['author'].strip()
    abstract = '' if 'abstract' not in data else data['abstract'].strip()
    titlebl = [] if 'titlebl' not in data or data['titlebl'] == '' else tentative_split(data['titlebl'], ',')
    authorbl = [] if 'authorbl' not in data or data['authorbl'] == '' else tentative_split(data['authorbl'], ',')
    abstractbl = [] if 'abstractbl' not in data or data['abstractbl'] == '' else tentative_split(data['abstractbl'], ',')
    prioritise = 'relevance' if 'prioritise' not in data or data['prioritise'] not in ALLOWED_PRIORITISE_ADV else data['prioritise']

    try:
        minyear = int(data['minyear'])
    except:
        minyear = 1996

    try:
        maxyear = int(data['maxyear'])
    except:
        maxyear = datetime.datetime.now().year

    try:
        page = int(data['page'])
    except:
        page = 0

    search_params = {
        'title': title,
        'titlebl': titlebl,
        'author': author,
        'authorbl': authorbl,
        'abstract': abstract,
        'abstractbl': abstractbl,
        'prioritise': prioritise,
        'minyear': minyear,
        'maxyear': maxyear,
        'page': page
    }

    papers, page, tot_pages = exec_advanced_search(search_params, backend="xapian")
    # for paper in papers:
    #     print(paper['title'], end="\n\n")

    if request.method == 'POST':
        return jsonify(papers)

    return render_papers(papers, page, tot_pages)
