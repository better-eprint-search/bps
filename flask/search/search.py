from sql_search import exec_generic_search_sql, exec_advanced_search_sql
from xapian_search import exec_generic_search_xapian, exec_advanced_search_xapian
try:
    from config import DEBUG
except ImportError:
    DEBUG = False

def exec_generic_search(search_params, backend="sql"):
    # handle special case of empty search string by using sql to return all results
    if search_params['searchstr'] == '':
        backend = "sql"

    if DEBUG: print(search_params)
    if DEBUG: print(f"searching using {backend} backend")

    if backend == "sql":
        return exec_generic_search_sql(search_params)
    elif backend == "xapian":
        return exec_generic_search_xapian(search_params)
    else:
        raise ValueError(f"backend `{backend}` not recognized")


def exec_advanced_search(search_params, backend="sql"):
    # handle special case of empty search string by using sql to return all results
    if search_params['title'] == '' \
        and search_params['author'] == '' \
        and search_params['abstract'] == '':
        backend = "sql"

    if DEBUG: print(search_params)
    if DEBUG: print(f"searching using {backend} backend")

    if backend == "sql":
        return exec_advanced_search_sql(search_params)
    elif backend == "xapian":
        return exec_advanced_search_xapian(search_params)
    else:
        raise ValueError(f"backend `{backend}` not recognized")
