// First, checks if `.format` it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
        ;
    });
  };
}

var gen_bibtex = function (event, authors, title, year, number) {
  event.preventDefault();

  // create bibtex entry
  var id = "{0}/{1}".format(year, number);
  var bib = "@misc{cryptoeprint:{0},\n".format(id);
  bib += "    author = {{0}},\n".format(authors.slice(0, authors.length - 5));
  bib += "    title = {{0}},\n".format(title);
  bib += "    howpublished = {Cryptology ePrint Archive, Paper {0}},\n".format(id);
  bib += "    year = {{0}},\n".format(year);
  bib += "    note = {\\url{https://eprint.iacr.org/{0}}},\n".format(id);
  bib += "    url = {https://eprint.iacr.org/{0}}\n".format(id);
  bib += "}"

  // copy entry to clipboard
  navigator.clipboard.writeText(bib)
  var backup_classname = event.target.className;
  event.target.className = "card-footer-item bibtex-link tooltip-big-text has-tooltip-hidden-tablet"
  // event.target.setAttribute("aria-label", "Copied to clipboard");

  // remove tooltip
  setTimeout(function () {
    // event.target.removeAttribute("aria-label");
    event.target.className = backup_classname;
  }, 1000);
}
