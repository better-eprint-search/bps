// pagination related functionality
document.addEventListener('DOMContentLoaded', () => {
  // parse GET parameters
  var $_GET = loadGET();

  // implementing openpage functionality for navbar
  openpage = function (page) {
    var url = '//' + location.host + location.pathname + '?page=' + (page-1);
    // preserve all keys except "page"
    for (var key in $_GET) {
      if (key == "page") {
        continue;
      }
      url += "&{0}={1}".format(encodeURIComponent(key), encodeURIComponent($_GET[key]));
    }
    window.location.href = url;
  }
  nextpage = function(page) {
    openpage(page + 1);
  }
  prevpage = function(page) {
    openpage(page - 1);
  }
});