function toggleAbstractMobile(el) {
  var div = el.previousElementSibling;
  var bq = el.parentElement;
  div.classList.toggle('hide-abstract');
  bq.classList.toggle('hide-abstract')
  if (el.innerText == "(show hidden)") {
    el.innerText = "(hide)";
  } else {
    el.innerText = "(show hidden)";
  }
}