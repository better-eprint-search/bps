var submit_search = function () {
  var searchstr = document.getElementById("searchstr").value;
  var field = document.getElementById("field").value;
  var prioritise_latest = document.getElementById("prioritise-latest").checked;
  var uri = '/search?';
  uri += 'searchstr=' + encodeURIComponent(searchstr);
  uri += '&field=' + encodeURIComponent(field);
  uri += '&prioritise-latest=' + encodeURIComponent(prioritise_latest);
  // const encoded = encodeURI(uri);
  window.location.href = uri;
}

document.addEventListener('DOMContentLoaded', () => {
  // have search happen by default on enter
  var press_search_btn = function (event) {
    if (event.key === "Enter") {
      event.preventDefault();
      document.getElementById("searchbtn").click();
    }
  };
  document.getElementById("searchstr").addEventListener("keypress", press_search_btn);
  document.getElementById("field").addEventListener("keypress", press_search_btn);

  // load search terms from previous search
  var $_GET = loadGET();
  if ('searchstr' in $_GET) {
    document.getElementById("searchstr").value = $_GET['searchstr'];
  }
  if ('field' in $_GET) {
    document.getElementById("field").value = $_GET['field'];
  }
  if ('prioritise-latest' in $_GET) {
    if ($_GET['prioritise-latest'] == 'true') {
      document.getElementById('prioritise-latest').checked = true;
    } else if ($_GET['prioritise-latest'] == 'false') {
      document.getElementById('prioritise-latest').checked = false;
    }
  }
});
