document.addEventListener('DOMContentLoaded', () => {
  // enable burger menu for tablet and mobile
  var el = document.getElementById("burger")
  el.addEventListener('click', () => {
    el.classList.toggle('is-active');
    document.getElementById("mobile-menu-container").classList.toggle('my-is-hidden');
  });
  mobile_adv_options = function () {
    document.getElementById("sidebar").classList.toggle('my-mobile-is-hidden');
  }
});