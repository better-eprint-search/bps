var submit_advanced_search = function () {
  // advanced search url composer
  var adv_title = document.getElementById("adv-title-search").value;
  var adv_author = document.getElementById("adv-author").value;
  var adv_abstract = document.getElementById("adv-abstract").value;
  var adv_title_blacklist = document.getElementById("adv-title-blacklist").value;
  var adv_author_blacklist = document.getElementById("adv-author-blacklist").value;
  var adv_abstract_blacklist = document.getElementById("adv-abstract-blacklist").value;
  var adv_min_year = parseInt(document.getElementById("adv-min-year").value);
  var adv_max_year = parseInt(document.getElementById("adv-max-year").value);
  if (adv_max_year < adv_min_year) {
    var temp = adv_max_year;
    adv_max_year = adv_min_year;
    adv_min_year = temp;
  }

  var uri = '/advanced?title=' + encodeURIComponent(adv_title);
  uri += '&titlebl=' + encodeURIComponent(adv_title_blacklist);
  uri += '&author=' + encodeURIComponent(adv_author);
  uri += '&authorbl=' + encodeURIComponent(adv_author_blacklist);
  uri += '&abstract=' + encodeURIComponent(adv_abstract);
  uri += '&abstractbl=' + encodeURIComponent(adv_abstract_blacklist);
  uri += '&minyear=' + encodeURIComponent(adv_min_year);
  uri += '&maxyear=' + encodeURIComponent(adv_max_year);

  var prioritise_nodes = document.getElementsByName("prioritise");
  var prioritise = "";
  for (var i = 0; i < prioritise_nodes.length; i++) {
    if (prioritise_nodes[i].checked) {
      prioritise = prioritise_nodes[i].value;
      break;
    }
  }
  uri += '&prioritise=' + encodeURIComponent(prioritise);

  // const encoded = encodeURI(uri);
  window.location.href = uri;
}

document.addEventListener('DOMContentLoaded', () => {
  // have search happen by default on enter
  var press_adv_search_btn = function (event) {
    if (event.key === "Enter") {
      event.preventDefault();
      document.getElementById("advsearchbtn").click();
    }
  };
  document.getElementById("adv-author").addEventListener("keypress", press_adv_search_btn);
  document.getElementById("adv-author-blacklist").addEventListener("keypress", press_adv_search_btn);
  document.getElementById("adv-title-search").addEventListener("keypress", press_adv_search_btn);
  document.getElementById("adv-title-blacklist").addEventListener("keypress", press_adv_search_btn);
  document.getElementById("adv-abstract").addEventListener("keypress", press_adv_search_btn);
  document.getElementById("adv-abstract-blacklist").addEventListener("keypress", press_adv_search_btn);
  document.getElementById("adv-min-year").addEventListener("keypress", press_adv_search_btn);
  document.getElementById("adv-max-year").addEventListener("keypress", press_adv_search_btn);

  // find out current year for year filter
  var year = new Date().getFullYear();
  document.getElementById("adv-min-year").setAttribute("max", year);
  document.getElementById("adv-max-year").setAttribute("max", year);
  document.getElementById("adv-max-year").value = year

  resetYears = function () {
    var year = new Date().getFullYear();
    document.getElementById("adv-min-year").value = 1996;
    document.getElementById("adv-max-year").value = year;
  }

  // load search terms from previous search
  var $_GET = loadGET();
  if ('title' in $_GET) {
    document.getElementById('adv-title-search').value = $_GET['title'];
  }
  if ('titlebl' in $_GET) {
    document.getElementById('adv-title-blacklist').value = $_GET['titlebl'];
  }
  if ('author' in $_GET) {
    document.getElementById('adv-author').value = $_GET['author'];
  }
  if ('authorbl' in $_GET) {
    document.getElementById('adv-author-blacklist').value = $_GET['authorbl'];
  }
  if ('abstract' in $_GET) {
    document.getElementById('adv-abstract').value = $_GET['abstract'];
  }
  if ('abstractbl' in $_GET) {
    document.getElementById('adv-abstract-blacklist').value = $_GET['abstractbl'];
  }
  if ('prioritise' in $_GET) {
    if ($_GET['prioritise'] == 'relevance') {
      document.getElementById('prioritise1').checked = true;
    } else if ($_GET['prioritise'] == 'newest') {
      document.getElementById('prioritise2').checked = true;
    } else if ($_GET['prioritise'] == 'oldest') {
      document.getElementById('prioritise3').checked = true;
    }
  }
  if ('minyear' in $_GET) {
    document.getElementById('adv-min-year').value = $_GET['minyear'];
  }
  if ('maxyear' in $_GET) {
    document.getElementById('adv-max-year').value = $_GET['maxyear'];
  }
});