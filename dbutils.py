import json
import sqlite3

# xapian in docker lives in the wrong subdir
import sys, os
alpine_python_path = '/usr/lib/python3.11/site-packages'
if os.path.exists(alpine_python_path):
    sys.path.append(alpine_python_path)
import xapian

from config import SQL_PAPERS_DB_PATH as sqlite_db
from config import XAPIAN_PAPERS_DB_PATH as xapian_db
try:
    from config import DEBUG
except ImportError:
    DEBUG = False

def setup_sqlite_db(dbpath=sqlite_db):
    con = sqlite3.connect(dbpath)
    cur = con.cursor()
    cur.execute("CREATE TABLE papers(\
        url TEXT unique,\
        year INTEGER,\
        number INTEGER,\
        title TEXT,\
        abstract TEXT,\
        subject TEXT,\
        authors JSON,\
        countries JSON,\
        first_uploaded TEXT,\
        last_revision TEXT,\
        filetype TEXT,\
        venue TEXT\
    )")
    con.close()


def get_db_connection(dbpath=sqlite_db):
    conn = sqlite3.connect(dbpath)
    conn.row_factory = sqlite3.Row
    return conn


def sanitise_paper_rows_generator(papers_rows):
    # parse authors as json
    for paper in papers_rows:
        p = dict(paper)
        p['authors'] = json.loads(paper['authors'])
        yield p


def sanitise_papers_rows(papers_rows, generator=False):
    gen = sanitise_paper_rows_generator(papers_rows)
    if generator:
        return gen
    else:
        return list(gen)


def count_search_res(sql_filter=None):
    # prepare query
    query = "SELECT COUNT(DISTINCT url) FROM papers"
    if sql_filter:
        query += " WHERE " + sql_filter

    # execute query
    conn = get_db_connection()
    count = conn.execute(query).fetchone()[0]
    conn.close()
    return count


def get_xapian_docid(db, paperid):
    postlist = db.postlist(f"Q{paperid}")
    try:
        plitem = next(postlist)
    except StopIteration:
        raise KeyError(f"No document with id {paperid}")
    return plitem.docid


def get_paper_unique_identifier(paper):
    # uid = f"{paper['year']}-{paper['number']}"
    uid = f"%d-%03d" % (int(paper['year']), int(paper['number']))
    return uid


def create_xapian_doc(paper):
    if isinstance(paper['authors'], str):
        authors = json.loads(paper['authors'])
    else:
        authors = paper['authors']

    # Set up a TermGenerator that we'll use in indexing.
    termgenerator = xapian.TermGenerator()
    termgenerator.set_stemmer(xapian.Stem("en"))

    # 'fields' is a dictionary mapping from field name to value.
    # Pick out the fields we're going to index.

    # We make a document and tell the term generator to use this.
    doc = xapian.Document()
    termgenerator.set_document(doc)

    # Index each field with a suitable prefix from https://xapian.org/docs/omega/termprefixes.html
    termgenerator.index_text(paper['title'], 1, 'S')
    termgenerator.index_text(paper['abstract'], 1, 'B')
    termgenerator.index_text(paper['subject'], 1, 'K')
    termgenerator.index_text(paper['url'], 1, 'U')
    termgenerator.index_text(", ".join([a['name'] for a in authors]), 1, 'A')
    termgenerator.index_text(", ".join([a['affiliations'] for a in authors]), 1, 'XF')
    termgenerator.index_text(paper['venue'], 1, 'XV')
    doc.add_value(0, xapian.sortable_serialise(int(paper['year']) * 100000 + int(paper['number'])))

    # terms for general, non-specific search
    termgenerator.index_text(paper['title'])
    termgenerator.increase_termpos()
    termgenerator.index_text(paper['abstract'])
    termgenerator.increase_termpos()
    termgenerator.index_text(paper['subject'])
    termgenerator.increase_termpos()
    termgenerator.index_text(paper['url'])
    termgenerator.increase_termpos()
    termgenerator.index_text(", ".join([a['name'] for a in authors]))
    termgenerator.increase_termpos()
    termgenerator.index_text(", ".join([a['affiliations'] for a in authors]))
    termgenerator.index_text(paper['venue'])

    # Store all the fields for display purposes.

    if not isinstance(paper['authors'], str):
        paper['authors'] = json.dumps(paper['authors'])

    doc.set_data(json.dumps(paper))
    # doc.set_data(json.dumps({
    #     'url': paper['url'],
    #     'title': paper['title'],
    #     'authors': json.dumps(paper['authors']),
    #     'abstract': paper['abstract'],
    #     'first_uploaded': paper['first_uploaded'],
    #     'last_revision': paper['last_revision'],
    #     'year': paper['year'],
    #     'number': paper['number'],
    #     'subject': paper['subject'],
    #     'countries': '',
    #     'filetype': paper['filetype']
    # }))

    # We use the identifier to ensure each object ends up in the
    # database only once no matter how many times we run the
    # indexer.
    idterm = u"Q" + get_paper_unique_identifier(paper)
    doc.add_boolean_term(idterm)
    return idterm, doc


def xapian_search(querystring, dbpath=xapian_db, offset=0, pagesize=10, logical_op=xapian.Query.OP_OR, prefixes=None, prioritise_by_year=False, prioritise=None):
    """
    :params offset:         defines starting point within result set
    :params pagesize:       defines number of records to retrieve

    :returns papers:

    EXAMPLE:
        if full results are [1, 2, 3, 4, 5],
            offset = 0, pagesize = 4 gives you [1, 2, 3, 4]
            offset = 1, pagesize = 4 gives you [2, 3, 4, 5]
    """

    # Open the database we're going to search.
    db = xapian.Database(dbpath)

    # Set up a QueryParser with a stemmer and suitable prefixes
    queryparser = xapian.QueryParser()
    queryparser.set_stemmer(xapian.Stem("en"))
    queryparser.set_stemming_strategy(queryparser.STEM_SOME)
    queryparser.set_default_op(logical_op)
    # default_op = queryparser.get_default_op()
    # print("default op:", default_op) # this is "OP_OR"

    # Start of prefix configuration.
    if isinstance(prefixes, list):
        if 'title' in prefixes:
            queryparser.add_prefix('title', 'S')
        if 'abstract' in prefixes:
            queryparser.add_prefix('abstract', 'B')
        if 'subject' in prefixes:
            queryparser.add_prefix('subject', 'K')
        if 'url' in prefixes:
            queryparser.add_prefix('url', 'U')
        if 'authors' in prefixes:
            queryparser.add_prefix('authors', 'A')
        if 'affiliations' in prefixes:
            queryparser.add_prefix('affiliations', 'XF')
    else:
        queryparser.add_prefix('title', 'S')
        queryparser.add_prefix('abstract', 'B')
        queryparser.add_prefix('subject', 'K')
        queryparser.add_prefix('url', 'U')
        queryparser.add_prefix('authors', 'A')
        queryparser.add_prefix('affiliations', 'XF')
    # End of prefix configuration.

    if DEBUG: print("adding range processor")
    queryparser.add_rangeprocessor(
        xapian.NumberRangeProcessor(0, "yy")
    )

    # And parse the query
    query = queryparser.parse_query(querystring)
    # print(dir(query))
    if DEBUG: print("query:", query)

    # Use an Enquire object on the database to run the query
    enquire = xapian.Enquire(db)
    enquire.set_query(query)
    if not prioritise or prioritise == "relevance":
        enquire.set_sort_by_relevance_then_value(0, True)
    elif prioritise == "newest":
        enquire.set_sort_by_value_then_relevance(0, True)
    elif prioritise == "oldest":
        enquire.set_sort_by_value_then_relevance(0, False)
    else:
        raise NotImplementedError(f"prioritising {prioritise} not understood")

    # if prioritise_by_year:
    #     enquire.set_sort_by_value_then_relevance(0, True)
    # else:
    #     enquire.set_sort_by_relevance_then_value(0, True)

    return enquire

    # And print out something about each match
    papers = []
    for match in enquire.get_mset(offset, pagesize):
        paper = json.loads(match.document.get_data().decode('utf8'))
        papers.append(paper)

    return papers
