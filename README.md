# Better ePrint Search (BPS)

A small alternative search engine for IACR ePrint publications. It implements most functions, and is able to search by author, title, abstract and time of first publication on the ePrint archive.

A live instance of BPS can be found at [search.indcpa.com](https://search.indcpa.com).

## Tech stack

The engine is contained and run inside an Alpine/Python docker image. Docker is not fully used "as intended". In particular, some software is installed inside Alpine when building the container.

The user interface is a Flask web application, with a Python backend handling the queries. The web application is served by Nginx. By default no configuration is offered for TLS, but this could be achieved by either changing the Nginx configuration or by using a reverse proxy on the host machine.

The data from ePrint is read via the Open Archives Initiative Protocol for Metadata Harvesting, [OAI-PMH](https://www.openarchives.org/pmh/), using the [pyoai](https://pypi.org/project/pyoai/) python module. While this retrieves most of the data on ePrint, it does not store affiliations and publication status, which currently are obtained using a scraping function.

The data is doubly stored in a SQLite database and in a [Xapian](https://xapian.org/) database. While the former allows familiar SQL queries with some pattern-matching, more general, less rigid text search is not trivial to implement. We use Xapian to perform these kind of searches.

Statistics of visitors are not taken by default when installing from this repository. However, there is support for capturing visitors staistitics via [Insights](https://getinsights.io/), and likely this could be relatively easily changed to support other similar services too.

## How to run

### Configuration files

The engine is relatively lightweight in terms of options.
These can be changed by momdifying `config.py`.
In particular, you may be interested in the following options:
- `MAX_COUNT`: this is the maximum numbers of papers to be pulled from ePrint. For a quick local run, one may want to significantly lower this number, since pulling papers takes a while.
- `STATISTICS_API_KEY`: if this variable is set to an [Insights](https://getinsights.io/) API token, then collection of visitor statistics will be enabled.
- `DEBUG`: If this variable is defined and set to `True`, some extra information about the queries being made is displayed to the terminal. This can be very useful when trying to debug SQL/Xapian queries.

### Creating and starting the container

To create the container, it is sufficient to run the following command in the repository's root directory:
```
bash build.sh
```
To run the container, similarly run
```
bash run.sh
```
As a result, the web app will be listening for connections on `localhost:5555`.

These shell scripts are just shortcuts to running Docker, and usual commands will work as expected.

During development, it can be convenient to run
```
bash build.sh && bash run.sh
```
to keep updating the server to the latest version of the code.

The docker container is setup to store persisting information in `/databases/`, including the SQLite and Xapian databases for the pulled ePrint data.
On first run, this directory will be empty. The directory will be automatically populated by a `cron` command with the following schedule (this can be changed in `Dockerfile`):
- Updates on the last 50 papers are fetched every 4 hours
- Updates on the last 5000 papers are fetched daily, at 2:31 am UTC time.
- Updates on the full list of papers since 1996 are fetched every 30 days, at 2:30 am UTC time.

Every time the pull is run, it will update entries in the database, if these already exist, and otherwise add them.
As [requested by IACR](https://eprint.iacr.org/rss/), users should not try to retrieve the archive more than once, daily. We try to keep our 4-hourly updates small (only 50 papers) to mostly satisfy this requirement, while keeping the database sufficiently fresh.

To manually start the retrieval process, it is possible to change the final commad in `Dockerfile` to `CMD [ "sh" ]`, to boot into a shell with `bash run.sh`. It is then possible to create the database by running `python oaipull.py` inside the container. 

Pulling the entire ePrint currently takes a few hours. For tests on smaller sets, change `MAX_COUNT` in `config.py`, or run `python oaipull.py -c COUNT` where `COUNT` is a small number.

Once a copy of the database was created, one can change the Dockerfile's last command to `CMD [ "sh", "run.sh" ]`, and run `bash build.sh && bash run.sh` to start the container.

Alternatively, the container can be run using `compose docker up -d`, by uncommenting the `nginx` lines, commenting `RUN echo 'python -m flask ...` and  `CMD [ "sh", "run.sh" ]`, and decommenting the `RUN chmod ...` and `ENTRYPOINT ...` lines in `Dockerfile`.

### Debugging

To debug the engine, the following changes to the setup can be useful:
- Set `DEBUG = True` in `config.py`.
- Set `CMD [ "sh" ]` as the last command in `Dockerfile`.
- Change `ENV FLASK_ENV production` into `ENV FLASK_ENV development` in the `Dockerfile`.
- Comment the commands under `# # configure nginx` in `Dockerfile`, and instead decomment `RUN echo 'python -m flask run --host=0.0.0.0' > run.sh`.
- Pulling the entire ePrint currently takes a few hours. For tests on smaller sets, change `MAX_COUNT` in `config.py`.

Then, refresh your running instance using
```
bash build.sh && bash run.sh
```

## License

The code is licensed under the GNU Lesser General Public License (LGPL) version 3.

## Possible future features

The following are features that could possibly be implemented (in no particular order):

- Enable search on main paper body.
- Enable filtering by last revised, publication venue, author institution.
- Try to guess author geolocation by institution, to possibly obtain statistics about most active countries/institutions.
- Add a "dark mode" feature.
- Add support for arXiv:cs.CR papers via [OAI](https://info.arxiv.org/help/oa/index.html).
- Consider having accents and other diacritics "flattened" to enable easier search of author names.
- Implement a safer version of `oaipull.py` that makes changes to temporary copies of the databases, and replaces the running ones only on confirmed successfull termination.

# Contributors

Fernando Virdia
