FROM python:3.11.6-alpine3.18

RUN apk update
RUN apk upgrade

# install requirements
RUN mkdir -p /better-eprint
COPY requirements.txt /better-eprint
RUN apk add alpine-sdk nginx
# RUN apk add openrc
RUN pip install -r /better-eprint/requirements.txt
RUN apk add xapian-bindings-python3
# tools to help debug
RUN apk add nano byobu

# copy source files
COPY oaipull.py /better-eprint
COPY dbutils.py /better-eprint
COPY config.py /better-eprint

# create a volume for holding the databases
VOLUME [ "/better-eprint/databases" ]

# the following will download updates from eprint at regular intervals

# pull last 50 papers for updates, every 4 hours
RUN echo '0 */4 * * * ( sh -c "cd /better-eprint/ && python /better-eprint/oaipull.py -c 50" ) ' >> /etc/crontabs/root

# pull last 5000 papers for updates, every saturday at 2:31 am UTC
RUN echo '31 2 * * 6 ( sh -c "cd /better-eprint/ && python /better-eprint/oaipull.py -c 5000" ) ' >> /etc/crontabs/root

# pull all papers for updates, every 30 days at 2:30 am UTC
RUN echo '30 2 */30 * * ( sh -c "cd /better-eprint/ && python /better-eprint/oaipull.py" ) ' >> /etc/crontabs/root

# setup frontend
COPY flask/search/wsgi.py /better-eprint/
COPY flask/search/app.py /better-eprint/
COPY flask/search/search.py /better-eprint/
COPY flask/search/sql_search.py /better-eprint/
COPY flask/search/xapian_search.py /better-eprint/
# COPY flask/search/traffic_stats.py /better-eprint/
RUN mkdir -p /better-eprint/templates
COPY flask/search/templates /better-eprint/templates
RUN mkdir -p /better-eprint/static
COPY flask/search/static /better-eprint/static

# Set environment variables.
ENV FLASK_APP app
ENV FLASK_ENV development
# ENV FLASK_ENV production

# work from project directory
WORKDIR /better-eprint

# # configure nginx
# COPY better-eprint.conf /etc/nginx/http.d/
# RUN echo "crond &" >> run.sh
# RUN echo "gunicorn -w 3 -b unix:better-eprint.sock -m 007 -u nginx wsgi:app &" >> run.sh
# RUN echo "nginx -g 'daemon off;'" >> run.sh
# # RUN echo "nginx" >> run.sh
# RUN echo "sh" >> run.sh

# # server run by flask
RUN echo 'python -m flask run --host=0.0.0.0' > run.sh
# CMD [ "python", "-m" , "flask", "run", "--host=0.0.0.0"]
# CMD [ "sh", "-c", "crond && python -m flask run --host=0.0.0.0" ]

# # server run by gunicorn
# RUN echo 'gunicorn -w 4 -b 0.0.0.0:5000 wsgi:app' > run.sh
# CMD [ "gunicorn", "-w", "4", "-b", "0.0.0.0:5000", "app:app" ]

# # boot to shell
# CMD [ "sh" ]

# # boot and start server and cron daemon
CMD [ "sh", "run.sh" ]

# For docker compose use the two commands below instead of the one just above
# Use the two commands below in place of the CMD 
# RUN chmod +x run.sh
# ENTRYPOINT "/better-eprint/run.sh"